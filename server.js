var express = require('express');
var router = express.Router();

// GET /add?x=5&y=7

router.get('/add', function(req,res,next) {
  res.send({resp:req.query.x + req.query.y});
});

module.exports = router;
